--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.15
-- Dumped by pg_dump version 11.2 (Debian 11.2-1.pgdg90+1)

-- Started on 2019-05-05 00:04:41 -05

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 181 (class 1259 OID 24576)
-- Name: acceso; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.acceso (
    correo character varying(100) NOT NULL,
    id_tipo integer NOT NULL,
    estado character(1) DEFAULT 'H'::bpchar NOT NULL
);


ALTER TABLE public.acceso OWNER TO postgres;

--
-- TOC entry 182 (class 1259 OID 24580)
-- Name: cajero; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cajero (
    denominacion character varying(3) NOT NULL,
    cantidad integer NOT NULL
);


ALTER TABLE public.cajero OWNER TO postgres;

--
-- TOC entry 183 (class 1259 OID 24583)
-- Name: tipo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tipo (
    id_tipo integer NOT NULL,
    nombre character varying(50)
);


ALTER TABLE public.tipo OWNER TO postgres;

--
-- TOC entry 184 (class 1259 OID 24586)
-- Name: tipo_id_tipo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tipo_id_tipo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tipo_id_tipo_seq OWNER TO postgres;

--
-- TOC entry 2134 (class 0 OID 0)
-- Dependencies: 184
-- Name: tipo_id_tipo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tipo_id_tipo_seq OWNED BY public.tipo.id_tipo;


--
-- TOC entry 185 (class 1259 OID 24588)
-- Name: usuario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.usuario (
    correo character varying(100) NOT NULL,
    clave character(32) NOT NULL,
    dinero integer NOT NULL
);


ALTER TABLE public.usuario OWNER TO postgres;

--
-- TOC entry 1998 (class 2604 OID 24591)
-- Name: tipo id_tipo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo ALTER COLUMN id_tipo SET DEFAULT nextval('public.tipo_id_tipo_seq'::regclass);


--
-- TOC entry 2123 (class 0 OID 24576)
-- Dependencies: 181
-- Data for Name: acceso; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.acceso (correo, id_tipo, estado) FROM stdin;
silviopd01@gmail.com	1	H
silviopd01@gmail.com	2	H
silviopd02@gmail.com	2	H
\.


--
-- TOC entry 2124 (class 0 OID 24580)
-- Dependencies: 182
-- Data for Name: cajero; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cajero (denominacion, cantidad) FROM stdin;
200	19
100	1
50	0
20	0
\.


--
-- TOC entry 2125 (class 0 OID 24583)
-- Dependencies: 183
-- Data for Name: tipo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tipo (id_tipo, nombre) FROM stdin;
1	Administrador
2	Cliente
\.


--
-- TOC entry 2127 (class 0 OID 24588)
-- Dependencies: 185
-- Data for Name: usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.usuario (correo, clave, dinero) FROM stdin;
silviopd02@gmail.com	202cb962ac59075b964b07152d234b70	1000
silviopd01@gmail.com	KK0l5xb2jtOh47RAXXTOEcQaqASYj7CM	320
\.


--
-- TOC entry 2135 (class 0 OID 0)
-- Dependencies: 184
-- Name: tipo_id_tipo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tipo_id_tipo_seq', 1, false);


--
-- TOC entry 2000 (class 2606 OID 24593)
-- Name: acceso pk_acceso_correo; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.acceso
    ADD CONSTRAINT pk_acceso_correo PRIMARY KEY (correo, id_tipo);


--
-- TOC entry 2002 (class 2606 OID 24595)
-- Name: cajero pk_cajero_denominacion; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cajero
    ADD CONSTRAINT pk_cajero_denominacion PRIMARY KEY (denominacion);


--
-- TOC entry 2004 (class 2606 OID 24597)
-- Name: tipo pk_tipo_id_tipo; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo
    ADD CONSTRAINT pk_tipo_id_tipo PRIMARY KEY (id_tipo);


--
-- TOC entry 2006 (class 2606 OID 24599)
-- Name: usuario pk_usuario_correo; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT pk_usuario_correo PRIMARY KEY (correo);


--
-- TOC entry 2007 (class 2606 OID 24600)
-- Name: acceso fk_acceso_tipo; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.acceso
    ADD CONSTRAINT fk_acceso_tipo FOREIGN KEY (id_tipo) REFERENCES public.tipo(id_tipo);


--
-- TOC entry 2008 (class 2606 OID 24605)
-- Name: acceso fk_acceso_usuario; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.acceso
    ADD CONSTRAINT fk_acceso_usuario FOREIGN KEY (correo) REFERENCES public.usuario(correo);


--
-- TOC entry 2133 (class 0 OID 0)
-- Dependencies: 7
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2019-05-05 00:04:41 -05

--
-- PostgreSQL database dump complete
--

