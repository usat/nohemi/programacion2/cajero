//condición para acceder al sistema helper.js
acceso()

//capturando los datos
opc1_billete_200 = document.getElementById("opc1_billete_200")
opc1_billete_100 = document.getElementById("opc1_billete_100")
opc1_billete_50 = document.getElementById("opc1_billete_50")
opc1_billete_20 = document.getElementById("opc1_billete_20")

opc2_billete_200 = document.getElementById("opc2_billete_200")
opc2_billete_100 = document.getElementById("opc2_billete_100")
opc2_billete_50 = document.getElementById("opc2_billete_50")
opc2_billete_20 = document.getElementById("opc2_billete_20")

//capturando el total en el cajero
function total() {
	let resultado = 0
	$.ajax({
		type: "POST",
		url: `../service/billetes_listar.php`,
		data: {},
		success: function(response) {
			let datos = JSON.parse(response)

			//funcion similar a un for para recorrer todos los datos que viene del servidor
			Object.keys(datos).map(keys => {
				//sumando las cantidades
				resultado += Number(datos[keys].denominacion) * datos[keys].cantidad

				//almacenando los billetes localmente con la funcion localStorage, se borra el almacenamiento cuando cierras la ventana (AKA 1)
				//se guarda su denominacion y la cantidad de billetes que tiene
				localStorage.setItem(datos[keys].denominacion, datos[keys].cantidad)
			})

			//guardando la data localmente (AKA 2)
			localStorage.setItem("total", resultado)
		}
	})

	//imprimiendo el daldo total que tiene el usuario que previamente lo hemos guardado localmente en localstorage
	document.getElementById("saldo_total").innerText = localStorage.getItem(
		"dinero"
	)

	// console.log(resultado)
	//retornamos el resultado de la suma total
	return resultado
}

function inicio() {
	//pintando los datos que esta localmente
	opc1_billete_200.innerHTML = 0
	opc1_billete_100.innerHTML = 0
	opc1_billete_50.innerHTML = 0
	opc1_billete_20.innerHTML = 0

	opc2_billete_200.innerHTML = 0
	opc2_billete_100.innerHTML = 0
	opc2_billete_50.innerHTML = 0
	opc2_billete_20.innerHTML = 0
}

//funcion para retirar dinero, se envia previamente el saldo(dinero a retirar) y el tipo(1 o 2)
function retirar(saldo, tipo) {
	//son los billetes que dara el cajero
	let b200 = 0
	let b100 = 0
	let b50 = 0
	let b20 = 0

	//son los billetes que tiene el cajero, que previamente lo hemos guardado localmente en la funcion total() (AKA 1)
	let cb200 = Number(localStorage.getItem("200"))
	let cb100 = Number(localStorage.getItem("100"))
	let cb50 = Number(localStorage.getItem("50"))
	let cb20 = Number(localStorage.getItem("20"))

	//data guardada localmente y se encuentra en la funcion total() (AKA 2)
	let cantidad = localStorage.getItem("total")

	//guardando previamente el dinero inicial
	let saldo_inicial = saldo

	//si el dinero a retirar es mayor a la cantidad del cajero entonces que retorne 1
	if (saldo > cantidad) return 1

	// si el dinero a retirar es mayor a tu saldo entonces que retorne 3
	if (saldo > localStorage.getItem("dinero")) return 3

	//si la opcion a retirar es 1 = retirar billetes de mayor denominación
	if (tipo == 1) {
		/* EXPLICACION 1 */
		//Math.floor(saldo / 200) redondea hacia abajo divine el dinero a retirar en 200
		// si Math.floor(saldo / 200) es menor a los billetes en el cajero(denominacion 200) y ademas hay billete(con la denominacion 200) en el cajero
		if (Math.floor(saldo / 200) <= cb200 && cb200 > 0) {
			//billetes a retirar(b200) es igual a la division del dinero a retirar entre 200 y además redondea hacia abajo
			//ejem: si retiro 500 entonces Math.floor(500/200) viene a ser igual a 2 ya que lo redondea hacia abajo
			b200 = Math.floor(saldo / 200)
			//el saldo que resta es el residuo de la division del dinero a retirar entre 200
			//ejemplo 500 % 200 el residuo viene a ser 100 ya que 500 entre 200 viene a ser 2 y el residuo es 100
			saldo = saldo % 200
		}
		// si Math.floor(saldo / 200) es mayor a los billetes en el cajero y además hay billetes en el cajero entonces:
		if (Math.floor(saldo / 200) > cb200 && cb200 > 0) {
			//los billetes que se va a retirar, solo va a ser el maximo de los billetes que se encuentra en el cajero
			b200 = cb200
			// y el saldo se resta de los billetes maximos que se va a retirar por la denominacion del billete (200) y todo esto se le resta al dinero a retirar
			saldo = saldo - cb200 * 200
		}

		// y todo esto se repide para las demás deniminación siempre y cuando el tipo sea 1
		/* EXPLICACION 1 */

		/* EXPLICACION 1 */
		if (Math.floor(saldo / 100) <= cb100 && cb100 > 0) {
			b100 = Math.floor(saldo / 100)
			saldo = saldo % 100
		}
		if (Math.floor(saldo / 100) > cb100 && cb100 > 0) {
			b100 = cb100
			saldo = saldo - cb100 * 100
		}
		/* EXPLICACION 1 */

		/* EXPLICACION 1 */
		if (Math.floor(saldo / 50) <= cb50 && cb50 > 0) {
			b50 = Math.floor(saldo / 50)
			saldo = saldo % 50
		}
		if (Math.floor(saldo / 50) > cb50 && cb50 > 0) {
			b50 = cb50
			saldo = saldo - cb50 * 50
		}
		/* EXPLICACION 1 */

		/* EXPLICACION 1 */
		if (Math.floor(saldo / 20) <= cb20 && cb20 > 0) {
			b20 = Math.floor(saldo / 20)
			saldo = saldo % 20
		}
		if (Math.floor(saldo / 20) > cb20 && cb20 > 0) {
			b20 = cb20
			saldo = saldo - cb20 * 20
		}
		/* EXPLICACION 1 */

		/* EXPLICACION 3 */
		//va a retornar 2 cuando tu quieras retirar dinero que no sea multiplo de 20
		//ejemplo si queremos retirar 21 es aqui cuando va a retornar 2
		// tambien va a retornar cuando el cajero si tenga dinero pero no de la denominacion que queremos
		//ejemplo en el cajero tenemos 200=1 100=1 50=0 y 20=0 si nosotros queremos retirar 20 es aqui cuando va a retornar 2 ya que el cajero si tiene esta cantidad pero no la denominacion del billete
		if (saldo > 0) return 2
		/* EXPLICACION 3 */
	} else if (tipo == 2) {
		/*EXPLICACION 2 */
		//cuando el tipo es 2 y se tiene que retirar de menor denominación
		//es parecido a cuando el tipo es igual a 1 solo se agrega un par de cosas más para validad

		//Revisar la explicación 1
		//En este caso se agrega una condicion más si el saldo es diferente a 0
		if (Math.floor(saldo / 20) <= cb20 && cb20 > 0 && saldo != 0) {
			b20 = Math.floor(saldo / 20)
			saldo = saldo % 20
		}
		if (Math.floor(saldo / 20) >= cb20 && cb20 > 0 && saldo != 0) {
			b20 = cb20
			saldo = saldo - cb20 * 20
		}

		//A diferencia de la explicación 1 es esta funcion de abajo
		//pd: saldo = dinero a retirar
		// pregunto que si el saldo que me queda, es menor a 50 y 100 y 200 y ademas el saldo es diferente a 0 que si hay saldo
		// ya que el residuo que queda no se puede retirar con otros billetes que no sea de 50,100 o 200
		if (saldo < 50 && saldo < 100 && saldo < 200 && saldo != 0) {
			//entonces yo quiero borrar lo que previamente hemos registrado y dejarlo en 0 billetes
			b20 = 0
			// y ademas restaurar el saldo para que el siguiente billete de denominación lo pueda retirar
			saldo = saldo_inicial
		}
		/*EXPLICACION 2 */

		/*EXPLICACION 2 */
		if (Math.floor(saldo / 50) <= cb50 && cb50 > 0 && saldo != 0) {
			b50 = Math.floor(saldo / 50)
			saldo = saldo % 50
		}
		if (Math.floor(saldo / 50) >= cb50 && cb50 > 0 && saldo != 0) {
			b50 = cb50
			saldo = saldo - cb50 * 50
		}
		if (saldo < 100 && saldo < 200 && saldo != 0 && saldo != 0) {
			b50 = 0
			saldo = saldo_inicial
		}
		/*EXPLICACION 2 */

		/*EXPLICACION 2 */
		if (Math.floor(saldo / 100) <= cb100 && cb100 > 0 && saldo != 0) {
			b100 = Math.floor(saldo / 100)
			saldo = saldo % 100
		}
		if (Math.floor(saldo / 100) >= cb100 && cb100 > 0 && saldo != 0) {
			b100 = cb100
			saldo = saldo - cb100 * 100
		}
		if (saldo < 200 && saldo != 0) {
			b100 = 0
			saldo = saldo_inicial
		}
		/*EXPLICACION 2 */

		/*EXPLICACION 2 */
		if (Math.floor(saldo / 200) <= cb200 && cb200 > 0 && saldo != 0) {
			b200 = Math.floor(saldo / 200)
			saldo = saldo % 200
		}
		if (Math.floor(saldo / 200) >= cb200 && cb200 > 0 && saldo != 0) {
			b200 = cb200
			saldo = saldo - cb200 * 200
		}
		/*EXPLICACION 2 */

		/* EXPLICACION 3 */
		if (saldo > 0) return 2
		/* EXPLICACION 3 */
	}

	// console.log(b200, b100, b50, b20)
	//retornamos los billetes qur se va a retirrar en un array
	return [b200, b100, b50, b20]
}

$("#formulario").submit(function(evento) {
	evento.preventDefault()

	//creando las variables de los billetes que se van a retirar
	let b200 = 0
	let b100 = 0
	let b50 = 0
	let b20 = 0
	let usuario = localStorage.getItem("email")

	let monto = document.getElementById("monto").value
	let opcion = document.getElementById("opcion").value

	if (monto.length == 0 || opcion.length == 0) {
		alert("ingrese datos, por favor")
		return
	}

	//almacenando el resultado que viene de la funcion retirar, recordemos que viene como array
	let resultado = retirar(Number(monto), opcion)
	console.log(resultado)

	if (resultado == 1) {
		alert(
			"no se puede retirar porque la cantidad a retirar es mayor a sus fondos"
		)
		return
	}

	if (resultado == 2) {
		alert("no se puede retirar")
		return
	}

	if (resultado == 3) {
		alert("no cuentas con tanto saldo")
		return
	}

	//recorriendo el array
	// la variable keys viene a ser como la i de un for(let i=0; i<resultado.length; i++)
	Object.keys(resultado).map(keys => {
		//si la key(posición) es 0 entonces quiere decir que retiraremos billetes de la denominacion de 200
		if (Number(keys) == 0) {
			b200 = Number(resultado[keys])
		}

		//si la key(posición) es 1 entonces quiere decir que retiraremos billetes de la denominacion de 100
		if (Number(keys) == 1) {
			b100 = Number(resultado[keys])
		}

		//si la key(posición) es 2 entonces quiere decir que retiraremos billetes de la denominacion de 50
		if (Number(keys) == 2) {
			b50 = Number(resultado[keys])
		}

		//si la key(posición) es 3 entonces quiere decir que retiraremos billetes de la denominacion de 20
		if (Number(keys) == 3) {
			b20 = Number(resultado[keys])
		}
	})

	/*EXPLICACION 4 */
	//despues enviamos al servidor todos los billetes y el usuario para restar el saldo al usuario y además restar esos billetes del cajero
	$.ajax({
		type: "POST",
		url: `../service/billetes_retirar.php`,
		data: { b200: b200, b100: b100, b50: b50, b20: b20, usuario: usuario },
		success: function(response) {
			console.log(response)

			if (response == 0) {
				alert(
					`se retiro correctamente correctamente: 200=${b200} 100=${b100} 50=${b50} 20=${b20}`
				)

				let retiro_total = b200 * 200 + b100 * 100 + b50 * 50 + b20 * 20
				var f = new Date()
				//instancia del pdf ha crear
				var doc = new jsPDF()

				//se crea el pdf
				doc.setFontSize(33)
				doc.writeText(0, 20, "Cajero Usat", { align: "center" })

				doc.setFontSize(15)
				doc.writeText(
					0,
					30,
					`Fecha: ${f.getDate()}/${f.getMonth() + 1}/${f.getFullYear()}`,
					{ align: "center" }
				)

				doc.setFontSize(18)
				doc.writeText(
					0,
					60,
					"--------------------- RETIRO ---------------------",
					{
						align: "center"
					}
				)

				doc.writeText(0, 70, `Cantidad a retirar es: ${retiro_total}`, {
					align: "center"
				})
				doc.writeText(
					0,
					80,
					`Su saldo anterior es: ${localStorage.getItem("dinero")}`,
					{
						align: "center"
					}
				)

				doc.writeText(
					0,
					90,
					`Su saldo actual es: ${Number(localStorage.getItem("dinero")) -
						retiro_total}`,
					{
						align: "center"
					}
				)

				doc.writeText(
					0,
					100,
					"--------------------- RETIRO ---------------------",
					{
						align: "center"
					}
				)

				doc.writeText(
					0,
					140,
					"--------- GRACIAS POR USAR NUESTRO CAJERO ---------",
					{
						align: "center"
					}
				)

				//retorna el pdf
				var pdf = doc.output()

				//convertimos el pdf en base64 para enviarlo por correo
				let comprobante = "data:application/pdf;base64," + Base64.encode(pdf)

				Email.send({
					SecureToken: "7f84f6f6-a8ea-474a-b724-205169dfe529",
					To: localStorage.getItem("email"),
					From: "programacionusat@gmail.com",
					Subject: "comprobante de retiro",
					Body: "comprobante del retiro",
					Attachments: [
						{
							name: "comprobante.pdf",
							data: comprobante
						}
					]
				}).then(message => {
					//enviando los datos al servidor y si responde OK entonces enviamos el dato al servidor para guardarlo en la bd
					if (message == "OK") {
						//enviando el pdf
						alert("se envio un comprobante a su correo")
					}
				})

				//almacenando el nuevo saldo que tiene el usuario
				localStorage.setItem("dinero", localStorage.getItem("dinero") - monto)

				//volver a reimprimir todo
				total()
				inicio()

				//borrando los datos de las cajas de monto y opcion
				document.getElementById("monto").value = ""
				document.getElementById("opcion").value = ""
			} else {
				alert("Error al registrar los datos!")
			}
		}
	})
})
/*EXPLICACION 4 */

//funcion similar a la /*EXPLICACION 4 */ con una pequeña diferencia
document.getElementById("btnVer").onclick = () => {
	b200 = 0
	b100 = 0
	b50 = 0
	b20 = 0

	let monto = document.getElementById("monto").value
	let opcion = document.getElementById("opcion").value

	if (monto.length == 0 || opcion.length == 0) {
		console.log("ingrese datos, por favor")
		return
	}

	let resultado = retirar(Number(monto), opcion)

	if (resultado == 1) {
		alert(
			"no se puede retirar porque la cantidad a retirar es mayor a sus fondos"
		)
		return
	}

	if (resultado == 2) {
		alert("no se puede retirar")
		return
	}

	if (resultado == 3) {
		alert("no cuentas con tanto saldo")
		return
	}

	Object.keys(resultado).map(keys => {
		if (Number(keys) == 0) {
			b200 = Number(resultado[keys])
		}

		if (Number(keys) == 1) {
			b100 = Number(resultado[keys])
		}

		if (Number(keys) == 2) {
			b50 = Number(resultado[keys])
		}

		if (Number(keys) == 3) {
			b20 = Number(resultado[keys])
		}
	})

	//que si la opcion es 1 quiero que me muestre los billetes de las denominaciones a retirar
	if (opcion == 1) {
		opc1_billete_200.innerHTML = b200
		opc1_billete_100.innerHTML = b100
		opc1_billete_50.innerHTML = b50
		opc1_billete_20.innerHTML = b20

		opc2_billete_200.innerHTML = 0
		opc2_billete_100.innerHTML = 0
		opc2_billete_50.innerHTML = 0
		opc2_billete_20.innerHTML = 0
	}

	//que si la opcion es 2 quiero que me muestre los billetes de las denominaciones a retirar
	if (opcion == 2) {
		opc1_billete_200.innerHTML = 0
		opc1_billete_100.innerHTML = 0
		opc1_billete_50.innerHTML = 0
		opc1_billete_20.innerHTML = 0

		opc2_billete_200.innerHTML = b200
		opc2_billete_100.innerHTML = b100
		opc2_billete_50.innerHTML = b50
		opc2_billete_20.innerHTML = b20
	}
}

//ejecutando todas estas funciones ni bien carga la web
total()
inicio()
