;(function(api, $) {
	"use strict"
	api.writeText = function(x, y, text, options) {
		options = options || {}

		var defaults = {
			align: "left",
			width: this.internal.pageSize.width
		}

		var settings = $.extend({}, defaults, options)

		// Get current font size
		var fontSize = this.internal.getFontSize()

		// Get the actual text's width
		/* You multiply the unit width of your string by your font size and divide
		 * by the internal scale factor. The division is necessary
		 * for the case where you use units other than 'pt' in the constructor
		 * of jsPDF.
		 */
		var txtWidth =
			(this.getStringUnitWidth(text) * fontSize) / this.internal.scaleFactor

		if (settings.align === "center") x += (settings.width - txtWidth) / 2
		else if (settings.align === "right") x += settings.width - txtWidth

		//default is 'left' alignment
		this.text(text, x, y)
	}
})(jsPDF.API, jQuery)

//funcion para generar la contraseña aleatoriamente
function aleatorio() {
	let chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	let lon = 32
	let code = ""

	for (let x = 0; x < lon; x++) {
		rand = Math.floor(Math.random() * chars.length)
		code += chars.substr(rand, 1)
	}

	return code
}

//borrando los datos localmente cuando cerramos sesión
function cerrar() {
	localStorage.removeItem("email")
	localStorage.removeItem("dinero")
	localStorage.removeItem("acceso")
	location.href = "login.html"
}

//funcion para acceder
function acceso() {
	//pregunto, si localmente no tengo mis variables email o dinero, entonces que me reenvie a login para logearme y acceder
	if (
		localStorage.getItem("email") == null ||
		localStorage.getItem("dinero") == null
	) {
		location.href = "login.html"
		return
	} else {
		//capturando el acceso localmente y convirtiendolo en json
		let acceso = JSON.parse(localStorage.getItem("acceso"))
		let acceso_tamaño = acceso.length

		let resultado = ""

		//preguntamos si el acceso tiene tamaño 1 y ademas el valor es 1 (Admoinistrador)
		if (acceso_tamaño == 1 && acceso[0] == 1) {
			resultado = `
			<li class="nav-item">
			<a class="nav-link" href="administrador.html">Administrador</a>
		</li>`
			//preguntamos si el acceso tiene tamaño 1 y ademas el valor es 2 (Usuario)
		} else if (acceso_tamaño == 1 && acceso[0] == 2) {
			resultado = `
								<li class="nav-item">
									<a class="nav-link" href="usuario.html">Usuario</a>
								</li>`
			//preguntamos si el acceso tiene tamaño 2 es porque tenemos acceso para ambos perfiles
		} else if (acceso_tamaño == 2) {
			resultado = `
									<li class="nav-item">
										<a class="nav-link" href="usuario.html">Usuario</a>
									</li>
										<li class="nav-item">
										<a class="nav-link" href="administrador.html">Administrador</a>
									</li>`
		}

		//imprimiendo el resultado en el header
		document.getElementById("cabecera").innerHTML = resultado
	}
}
