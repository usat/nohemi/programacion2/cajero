document.getElementById("btnEnviar").onclick = () => {
	let email = document.getElementById("txtemail").value //capturando el email
	let password = aleatorio() //password aleatorio que viene de js/helper.js

	/* Aqui se envia al correo, más información en https://www.smtpjs.com/ */
	Email.send({
		SecureToken: "7f84f6f6-a8ea-474a-b724-205169dfe529",
		To: `${email}`,
		From: "programacionusat@gmail.com",
		Subject: "acceso al sistema",
		Body: `${password}`
	}).then(message => {
		//enviando los datos al servidor y si responde OK entonces enviamos el dato al servidor para guardarlo en la bd
		if (message == "OK") {
			//enviando los datos email y contraseña
			$.ajax({
				type: "POST",
				url: "../service/registrarAcceso.php",
				data: {
					email: email,
					password: password
				},
				success: function(response) {
					console.log(response)
					if (response == 0) {
						alert("Se envio a su correo el acceso")
					} else {
						alert("Error al registrar los datos!")
					}
				}
			})
		}
	})
}

$("#formulario").submit(function(evento) {
	evento.preventDefault()

	//creando el array del acceso donde se va a pintar el header
	let acceso = []

	//capturando los datos
	let email = document.getElementById("txtemail").value
	let password = $("#txtpassword").val()

	// console.log(email, password)

	$.ajax({
		type: "POST",
		url: "../service/acceso.php",
		data: {
			email: email,
			password: password
		},
		success: function(response) {
			//convirtiendo la respuesta del servidor
			let datos = JSON.parse(response)

			// console.log(datos)
			// si los datos que viene del servidor es vacio entonces no sé procederá al sistema
			if (datos.length == 0) {
				alert("no se encontro este usuario o contraseña incorrecta")
				return
			}

			//si la cantidad de datos, es igual a uno agregamos al array el tipo
			if (datos.length == 1) {
				acceso.push(datos[0].id_tipo)
			}

			//si la cantidad de datos, es igual a dos agregamos al array el tipo
			if (datos.length == 2) {
				acceso.push(datos[0].id_tipo)
				acceso.push(datos[1].id_tipo)
			}

			//con localstorage se guarda localmente en la pagina para despues usarlo, en este caso estamos guardando el email,
			//el dinero que tiene el usuario y el acceso que tiene a ellas para despues usarlo
			localStorage.setItem("email", datos[0].correo)
			localStorage.setItem("dinero", datos[0].dinero)
			localStorage.setItem("acceso", JSON.stringify(acceso))

			//si en la posicion 1 del array es igual a 1 entonces que nos envie a administrador, en caso contrario nos envie a usuario
			if (datos[0] == 1) {
				location.href = "administrador.html"
			} else {
				location.href = "usuario.html"
			}
		}
	})
})
