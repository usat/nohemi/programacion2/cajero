//condición para acceder al sistema helper.js
acceso()

//capturando datos
billete_200 = document.getElementById("billete_200")
billete_100 = document.getElementById("billete_100")
billete_50 = document.getElementById("billete_50")
billete_20 = document.getElementById("billete_20")

//capturando el total en el cajero
function total() {
	let resultado = 0
	$.ajax({
		type: "POST",
		url: `../service/billetes_listar.php`,
		data: {},
		success: function(response) {
			let datos = JSON.parse(response)

			//funcion similar a un for para recorrer todos los datos que viene del servidor
			Object.keys(datos).map(keys => {
				//sumando las cantidades
				resultado += Number(datos[keys].denominacion) * datos[keys].cantidad

				//almacenando los billetes localmente con la funcion localStorage, se borra el almacenamiento cuando cierras la ventana
				//se guarda su denominacion y la cantidad de billetes que tiene
				localStorage.setItem(datos[keys].denominacion, datos[keys].cantidad)
			})

			//assignando el monto
			document.getElementById("monto_total").innerHTML = `$ ${resultado}`

			//guardando la data localmente
			localStorage.setItem(total, resultado)

			inicio()
		}
	})

	// console.log(resultado)
	//retornamos el resultado de la suma total
	return resultado
}

function inicio() {
	//pintando los datos que esta localmente
	billete_200.innerHTML = localStorage.getItem("200")
	billete_100.innerHTML = localStorage.getItem("100")
	billete_50.innerHTML = localStorage.getItem("50")
	billete_20.innerHTML = localStorage.getItem("20")
}

//ejecutando la funcion total ni bien se ingresa a la web
total()

$("#formulario").submit(function(evento) {
	evento.preventDefault()

	//capturando los datos para enviarlo al servidor y despues actualizar con la bd
	b200 = document.getElementById("input_billete_200").value
	b100 = document.getElementById("input_billete_100").value
	b50 = document.getElementById("input_billete_50").value
	b20 = document.getElementById("input_billete_20").value

	$.ajax({
		type: "POST",
		url: `../service/billetes_agregar.php`,
		data: { b200: b200, b100: b100, b50: b50, b20: b20 },
		success: function(response) {
			console.log(response)

			if (response == 0) {
				alert(`se agrego al cajero correctamente`)
				total()
			} else {
				alert("Error al registrar los datos!")
			}
		}
	})
})
